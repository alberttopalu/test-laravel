<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Post Title</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <!-- Styles -->
    </head>
    <body>

    <div class="container">
        <div class="row">
            <div class="col-md-12" style="border: 1px solid black; margin-top: 5%">
                <a href="/"><h3 >Index Page</h3></a>
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                @foreach($posts as  $post)

                    <h3 style="text-align: center" >{{$post['title']}}</h3><br>
                    <div class="col-md-10 col-md-offset-1" style="background-color: #eeeeee;padding: 1%; margin-bottom: 2%">

                        <div class="col-md-4" style="background-color: #2ab27b; height: 200px;"></div>
                        <div class="col-md-8" >{{$post['description']}}</div>
                    </div>


                    {{----------------------Comment form---------------------------}}
                    <div  class="col-md-10 col-md-offset-1 " >
                        <form class="form-horizontal " method="post" action="{{url('/post/{id}')}}">

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="id" value="{{$post['id']}}">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Comment</label>

                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">name</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="commentName" min="1" max="50" required >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">email</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="commentEmail" min="1" max="50" required >
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">comment</label>
                                <div class="col-md-6">
                                    <textarea name="commentText"  class="form-control" cols="40" rows="3" required ></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        comment
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{----------------------End Comment Form---------------------------}}


                <div class="col-md-10 col-md-offset-1" style="background-color: #cccccc; height: 100%; margin-bottom: 20px;">

                  {{--{{dd($posts)}}--}}
                    <div class="col-md-10 col-md-offset-1">
                        <h3>Comments:</h3>
                        @foreach($post['comment_action'] as $comment)

                            <div class="col-md-12" style="border: 1px solid black; margin-bottom: 5px">
                                <div class="col-md-10 pull-left">
                                    <p style="font-size: 18px"><b>Name&nbsp; - &nbsp;</b>{{$comment['name']}}</p>
                                    <p style="font-size: 18px"><b>Email&nbsp; - &nbsp;</b>{{$comment['email']}}</p>
                                    <p style="font-size: 18px"><b>Comment&nbsp; - &nbsp;</b>{{$comment['comment']}}</p>
                                </div>
                            </div>
                            <br>
                        @endforeach
                    </div>


                </div>
                @endforeach
            </div>
        </div>
    </div>



    {{--<div class="flex-center position-ref full-height">

            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>--}}
    </body>
</html>
