<?php

namespace App\Http\Controllers;


use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $posts = Post::paginate(2);
        return view('/welcome', compact('posts'));
    }

    public function post($id)
    {
//        $id = request()->route('id');
        $posts = Post::with('commentAction')

        ->where('id',$id)->get()->toArray();

        return view('/post', compact('posts'));
    }

    public function addComment(Request $request)
    {
        $comment = new Comment();
        $comment->post_id = $request->id;
        $comment->name = $request->commentName;
        $comment->email = $request->commentEmail;
        $comment->comment = $request->commentText;

        $comment->save();
        return redirect('/post/'.$request->id)->with('message', 'send comment!');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
