<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->get('/post', 'HomeController@post');
    $router->post('/post', 'HomeController@newPost');


    $router->post('/update/comment', 'HomeController@updateComment');
    $router->post('/update/comment/form', 'HomeController@updateCommentForm');
    $router->post('/delete/comment', 'HomeController@deleteComment');

});
