<?php

namespace App\Admin\Controllers;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Post;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form\Field\Id;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    public function index()
    {

        return Admin::content( function (Content $content) {

            $content->header('Dashboard');
            $content->description('Description...');

            $content->row(function (Row $row) {

                $row->column(12, function (Column $column) {
                    $posts = Post::with('comments')->get()->toArray();

                    $column->append(Dashboard::extensions()->with('posts',$posts));
                });

//
//                $row->column(4, function (Column $column) {
//                    $column->append(Dashboard::dependencies());
//                });
            });
        });
    }

    public function post()  {


        return Admin::content(function (Content $content) {

            $content->row(function (Row $row) {

                $row->column(12, function (Column $column) {

                    $column->append(Dashboard::environment());
                });

            });
        });
    }

    public function newPost(Request $request){

        $newPost = new Post();
        $newPost->title = $request->PostNewTitle;
        $newPost->description = $request->PostNewText;
        $newPost->img = $request->PostNewImg;

//        $file = Input::file('file');
//        $newPost = $request->file ;

//        $file = Input::file('PostNewImg');
//        $destinationPath = 'public/img/';
//        $file->getClientOriginalName();
//        Input::file('PostNewImg')->move($destinationPath, $file);



        $newPost->save();
        return redirect('admin/post/')->with('message', 'ADD NEW POST!');
    }

    public function updateComment(Request $request){

        if($request->commentId){
            $updateComment = Comment::find($request->commentId);
            $updateComment->action = $request->optradio;
            $updateComment->save();
        }

      return redirect('admin/')->with('message', 'ok!');
    }


    public function deleteComment(Request $request){

        if($request->commentDelete){
//            $commentDelete = Comment::find($request->commentDelete)->delete();
            Comment::where('id',$request->commentDelete)->delete();
//            return back();
        }

        return redirect('admin/')->with('message', 'delete!');
    }

    public function updateCommentForm(Request $request){

        if($request->commentId){
            $updateComment = Comment::find($request->commentId);
            $updateComment->name = $request->editName;
            $updateComment->email = $request->editEmail;
            $updateComment->comment = $request->editComment;
            $updateComment->save();
        }

        return redirect('admin/')->with('message', 'ok!');
    }



}
