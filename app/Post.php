<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function commentAction()
    {
        return $this->hasMany(Comment::class)->where('action','1');
    }
}
